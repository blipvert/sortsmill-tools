/*
 * Copyright (C) 2013 Barry Schwartz
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SORTSMILL_MATH_H
#define _SORTSMILL_MATH_H

#include <sortsmill/math/bincoef.h>
#include <sortsmill/math/brentroot.h>
#include <sortsmill/math/gmp_constants.h>
#include <sortsmill/math/polyspline.h>  /* FIXME: Make polyspline.h
                                           hierarchical. */
#include <sortsmill/math/polyspline/bases.h>

#include <sortsmill/math/f64_matrix.h>
#include <sortsmill/math/gmp_matrix.h>
#include <sortsmill/math/scm_matrix.h>
#include <sortsmill/math/transmatrix.h>

#endif /* _SORTSMILL_MATH_H */
