/*
 * Copyright (C) 2012 by Barry Schwartz
  
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.

 * Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.

 * The name of the author may not be used to endorse or promote products
 * derived from this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _SORTSMILL_XUNICASE_H
#define _SORTSMILL_XUNICASE_H

#include <unicase.h>

#ifdef __cplusplus
extern "C"
{
#endif
#if 0
}
#endif

int u8_casecompare (const uint8_t *s1, const uint8_t *s2);
int u16_casecompare (const uint16_t *s1, const uint16_t *s2);
int u32_casecompare (const uint32_t *s1, const uint32_t *s2);

/* The following routines count numbers of storage units rather than
   multibyte characters. (They are appropriate, for example, where 'n'
   equals the difference of two pointers.) */
int u8_ncasecompare (const uint8_t *s1, const uint8_t *s2, size_t n);
int u16_ncasecompare (const uint16_t *s1, const uint16_t *s2, size_t n);
int u32_ncasecompare (const uint32_t *s1, const uint32_t *s2, size_t n);

#if 0
{
#endif
#ifdef __cplusplus
}
#endif

#endif /* _SORTSMILL_XUNICASE_H */
