# -*- tab-width: 4 -*-

# Copyright (C) 2000-2012 by George Williams
# Copyright (C) 2012 by Barry Schwartz
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# The name of the author may not be used to endorse or promote products
# derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

include $(top_srcdir)/mk/layout.am

AM_CPPFLAGS = -I$(top_builddir)/lib -I$(top_srcdir)/lib	\
	-I$(top_builddir)/inc -I$(top_srcdir)/inc			\
	-DSHAREDIR=\"${MY_SHAREDIR}\" 						\
	-DDOCDIR=\"${MY_DOCDIR}\"							\
	-DLOCALEDIR=\"${MY_LOCALEDIR}\"						\
	-DFF_TEXTDOMAIN=\"${FF_TEXTDOMAIN}\"				\
	-DFF_SHORTCUTSDOMAIN=\"${FF_SHORTCUTSDOMAIN}\"		\
	-DFF_MACSHORTCUTSDOMAIN=\"${FF_MACSHORTCUTSDOMAIN}\"\
	-DVERSION_MAJOR=\"${VERSION_MAJOR}\"				\
	-DVERSION_MINOR=\"${VERSION_MINOR}\"				\
	-DVERSION_PATCH=\"${VERSION_PATCH}\"				\
	-DVERSION_EXTRA=\"${VERSION_EXTRA}\"				\
	-DVERSION_EXTRA_SHORT=\"${VERSION_EXTRA_SHORT}\"	\
	-DD_pixmapsdir=\"${pixmapsdir}\"					\
	-DD_cursorsdir=\"${cursorsdir}\"					\
	-DD_htdocsdir=\"${htdocsdir}\"						\
	-DD_pkgconfigdir=\"${pkgconfigdir}\"				\
	-DD_pkgguiledatadir=\"${pkgguiledatadir}\"			\
	-DD_guilemoduledir=\"${guilemoduledir}\"			\
	-DD_guileobjmoduledir=\"${guileobjmoduledir}\"		\
	-DD_cythonincludedir=\"${cythonincludedir}\"		\
	-DD_fcmoduleincludedir=\"${fcmoduleincludedir}\"	\
	-DD_pkgdatadir=\"${pkgdatadir}\"					\
	-DD_localedir=\"${localedir}\"						\
	-DD_sysconfdir=\"${sysconfdir}\"					\
	$(MY_CFLAGS)

FFINCLDIRS = -I$(top_builddir)/fontforge -I$(top_srcdir)/fontforge

AM_CFLAGS = $(CFLAG_VISIBILITY)

GNULIB_LD_FLAGS =		\
	$(ATAN2_LIBM)		\
	$(COS_LIBM)			\
	$(FABS_LIBM)		\
	$(FLOOR_LIBM)		\
	$(FMA_LIBM)			\
	$(FREXP_LIBM)		\
	$(GETHOSTNAME_LIB)	\
	$(HOSTENT_LIB)		\
	$(HYPOT_LIBM)		\
	$(LDEXP_LIBM)		\
	$(LIBSOCKET)		\
	$(LIB_SELECT)		\
	$(LTLIBICONV)		\
	$(LTLIBINTL)		\
	$(LTLIBTHREAD)		\
	$(LTLIBUNISTRING)	\
	$(POW_LIBM)			\
	$(RINT_LIBM)		\
	$(SIN_LIBM)			\
	$(SQRT_LIBM)		\
	$(TAN_LIBM)			\
	@INTL_MACOSX_LIBS@

# Using -release instead of -version-info seems appropriate in the
# absence of a well defined ABI.
COMMON_LD_FLAGS = -prefer-pic -release $(VERSION) $(MY_CFLAGS)	\
	$(MY_LIBS) $(GNULIB_LD_FLAGS)

ldflags = $(COMMON_LD_FLAGS)
